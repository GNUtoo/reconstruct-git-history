#!/usr/bin/env python
# Copyright (C) 2018 Denis 'GNUtoo' Carikli <GNUtoo@makefreedom.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup, find_packages
setup(
    name="reconstruct-git-history",
    version="0.0.1",
    packages=find_packages(),
    scripts=['reconstruct-git-history.py'],

    # metadata for upload to PyPI
    author="Denis 'GNUtoo' Carikli",
    author_email="GNUtoo@no-log.org",
    description="This can be used to reconstruct git history in some cases",
    license="GPLv3+",
)
