#!/bin/bash
# Copyright (C) 2018 Denis 'GNUtoo' Carikli <GNUtoo@makefreedom.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

upstream_repo="upstream-git"
soc_repo="soc-git"
final_repo="final-git"
soc_tarball="$(basename ${soc_repo})_archive.tar"
device_repo="device_dir"
device_tarball="$(basename ${device_repo})_archive.tar"
test_log_file="test.log"
final_results_log_file="results.log"

nr_files=6 # Has to be a multiple of 3
depth=5

set_file()
{
    file="$1"
    text="$2"
    num="$3"
    echo "${text} ${num}" > ${file}
}

get_text()
{
   file="$1"
   # File format: <text> <num>
   text=$(cat ${file} | \
	      awk '{for (i = 1; i < NF - 1; ++i) {print $i FS} print $(NF-1)}')
   echo "${text}"
}

get_num()
{
    file="$1"
    # File format: <text> <num>
    num=$(cat ${file} | awk '{print $NF}')
    echo "${num}"
}

increment_file()
{
    file="$1"
    # File format: <text> <num>
    num=$(get_num ${file})
    text=$(get_text ${file})
    echo "${text} $(expr ${num} + 1)" > ${file}
}

multiply_file()
{
    file="$1"
    # File format: <text> <num>
    num=$(cat ${file} | awk '{print $NF}')
    text=$(cat ${file} | awk '{for (i = 1; i < NF; ++i) print $i FS}')
    echo "${text} $(expr ${num} \* 2)" > ${file}
}

add_changes()
{
    repo_type="$1"
    dir="$2"
    text="$3"
    change_type="$4"
    files_start="$5"
    files_end="$6"
    passes_start="$7"
    passes_end="$8"

    files_created=0

    printf "Add changes: repo_type %s dir %s text %s change_type %s files_start %s files_end %s passes_start %s passes_end %s\n" ${repo_type} ${dir} ${text} ${change_type} ${files_start} ${files_end} ${passes_start} ${passes_end} 
    
    # Create the files if they are not there
    for filenum in $(seq ${files_start} ${files_end}) ; do
	if [ ! -f ${dir}/${filenum} ] ; then
	    echo "${text} ${filenum}" > ${dir}/${filenum}
	    if [ "${repo_type}" = "git" ] ; then
	       	    git -C ${dir} add ${filenum}
	    fi
	    files_created=1
	    echo "Files created"
	fi
    done

    if [ "${files_created}" = "1" -a "${repo_type}" == "git" ] ; then
	    git -C ${dir}/ commit -asm "${text} 0"
	    echo "First commit"
    fi

    for passnum in $(seq ${passes_start} ${passes_end}) ; do
	for filenum in $(seq ${files_start} ${files_end}) ; do
	    old_text=$(get_text ${dir}/${filenum})
	    if [ "${change_type}" != "increment" -a "${change_type}" != "multiply" ] ; then
		echo "${change_type} not implemented"
		exit 1
	    fi
	    
	    if [ "${change_type}" = "increment" -a "${old_text}" = "${text}" ] ; then
		increment_file ${dir}/${filenum}
		echo "increment file done: ${dir}/${filenum}: $(cat ${dir}/${filenum})"
	    elif [ "${change_type}" = "multiply" -a "${old_text}" = "${text}" ] ; then
		multiply_file ${dir}/${filenum}
		echo "multiply file done: ${dir}/${filenum}: $(cat ${dir}/${filenum})"
	    elif [ "${old_text}" != "${text}" ] ; then
		set_file ${dir}/${filenum} "${text}" 1
		echo "set file done: ${dir}/${filenum}: $(cat ${dir}/${filenum})"
	    fi

	    if [ "${repo_type}" = "git" ] ; then
		git -C ${dir} add ${filenum}
		echo "Add ${dir}/${filenum}"
	    fi
	done

	if [ "${repo_type}" == "git" ] ; then
	    # don't error in case we have nothing to add
	    git -C ${dir}/ commit -asm "${text} ${passnum}" || true
	    echo "Commit changes from ${dir}"
	fi
    done
}

release_tarball_from_git()
{
    repo="$1"
    tarball="$2"

    git -C ${repo} archive --format=tar -o ../${tarball} HEAD
}

release_tarball_from_dir()
{
    dir="$1"
    tarball="$2"

    tar -C ${dir} -cf ${tarball} ./
}

extract_tarball()
{
    tarball_path="$1"
    extract_dir="$2"

    mkdir -p ${extract_dir}
    tar xf ${tarball_path} -C ${extract_dir}
}

clean()
{
    rm -f ${soc_tarball}
    rm -f ${device_tarball}

    rm -rf ${upstream_repo}
    rm -rf ${soc_repo}
    rm -rf ${device_repo}
    rm -rf ${final_repo}
}

create_test_source()
{
    mkdir -p ${upstream_repo}
    git -C ${upstream_repo} init
    git -C ${upstream_repo} commit -asm "Initial import" || true

    # Make some upstream changes
    add_changes git ${upstream_repo} "Upstream" increment \
		1 ${nr_files} \
		1 ${depth}
    git -C ${upstream_repo} tag v${depth} HEAD

    # Now use the tag version as a base for the SOC vendor
    cp -r ${upstream_repo} ${soc_repo}

    # Meanwhile upstream makes more changes upstream and tag them
    add_changes git "${upstream_repo}" "Upstream" increment \
		1 ${nr_files} \
		$(expr ${depth} + 1 ) $(expr ${depth} \* 2)
    git -C ${upstream_repo} tag v$(expr ${depth} \* 2) HEAD

    # and the SOC vendor also makes its changes changes
    # The SOC vendor doesn't change all files only the 2/3 of the files
    # that have the biggest filename number
    echo "<#####################################################################################"
    echo \
    add_changes git "${soc_repo}" "SOC" increment \
		$(expr $(expr ${nr_files} \/ 3) + 1) ${nr_files} \
		1 ${depth}

    add_changes git "${soc_repo}" "SOC" increment \
		$(expr $(expr ${nr_files} \/ 3) + 1) ${nr_files} \
		1 ${depth}
    echo "#####################################################################################>"
    # Then the SOC vendor releases a tarball that the device vendor will use
    # The device vendor will not tell us which soc tarball they used
    release_tarball_from_git "${soc_repo}" "${soc_tarball}"

    # The SOC vendor continues to make some changes after the soc tarball release
    add_changes git "${soc_repo}" "SOC" increment \
		$(expr $(expr ${nr_files} \/ 3) + 1) ${nr_files} \
		$(expr ${depth} + 1) $(expr ${depth} \* 2)

    ######################
    # Device vendor code #
    ######################

    # extract the soc tarball
    mkdir -p ${device_repo}
    extract_tarball ${soc_tarball} ${device_repo}

    # Add device vendor changes on top
    add_changes tarball ${device_repo} "device" increment \
		$(expr $(expr $(expr ${nr_files} \* 2) \/ 3) + 1) ${nr_files} \
		1 ${depth}
    rm -f ${soc_tarball}

    # Release a device vendor tarball
    release_tarball_from_dir "${device_repo}" "${device_tarball}"
}

test_recreate_history()
{
    # first get the upstream repo and checkout the right base version
    cp -r ${upstream_repo} ${final_repo}
    git -C ${final_repo} checkout v${depth} -b device

    # Then apply the device vendor tarball on top
    extract_tarball ${device_tarball} ${final_repo}

    git -C ${final_repo} commit -asm "Import device vendor changes"

    # Then add the SOC vendor git remote
    git -C ${final_repo} remote add soc ../${soc_repo}/.git/
    git -C ${final_repo} fetch soc

    # And try to recontruct the history
    cd ${final_repo}
    ../../reconstruct-git-history/reconstruct-git-history.py -v -t v${depth} -r soc | tee ../${test_log_file}
}

usage()
{
    echo "usage:"
    echo "$0 test"
    echo "$0 clean"
    exit 1
}

if [ $# -eq 0 ] ; then
    set -x
    clean
    create_test_source
    test_recreate_history
elif [ $# -eq 1 -a "$1" = "clean" ] ; then
    set -x
    clean
else
    usage
fi
