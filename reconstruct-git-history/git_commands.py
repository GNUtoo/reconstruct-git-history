#!/usr/bin/env python2
# Copyright (C) 2018 Denis 'GNUtoo' Carikli <GNUtoo@makefreedom.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import copy
import time
from git import Repo

# We use git directly because we have this error:
# TypeError: PackingType of packed-Refs not understood: '# pack-refs with: peeled fully-peeled sorted'
# This is with the following versions:
# - git: 2.17.0
# - gitdb > 2.0.3: 3e71833 (HEAD -> master, origin/master, origin/HEAD) Merge pull request #44 from movermeyer/fix_badges
# - gitpython > 2.1.9: c6e0a6c (HEAD -> master, origin/master, origin/HEAD) Avoid from_timestamp() function to raise an exception when the offset is greater or lower than 24 hours.
# Depending on code that might break when git repositories format changes is also not a very good idea
# unless this code is optional

""" git commands wrapper """
class git_commands():
    def __init__(self, repo_dir=None):
        self.git = None
        if repo_dir:
            self.git = Repo(repo_dir).git
        else:
            self.git = Repo(os.curdir).git

    """ Returns a list of paths of files that differs between commit1 and commit2 """
    def list_diff_files(self, commit1, commit2):
        return unicode(self.git.diff('--name-only', commit1, commit2)).split(os.linesep)

    """ Returns a list of paths touched by commit1
        if commit2 is given, it will return a list of all path touched by both commits
        and all the commits in between
    """
    def list_commit_files(self, commit1, commit2=None):
        if commit2:
            return unicode(self.git.log('--oneline', '--pretty=tformat:', '--name-only',
                                        commit1, commit2)).split(os.linesep)
        else:
            return unicode(self.git.log('--oneline', '--pretty=tformat:', '--name-only',
                                        commit1)).split(os.linesep)

    """List git branches """
    def list_branches(self, remote):
        return unicode(self.git.branch('-r', '--list',
                          '{}/*'.format(remote))).replace(' ', '').split(os.linesep)

    """List git branches containing a given tag """
    def list_branches_with_tag(self, remote, tag):
        return unicode(self.git.branch('-r', '--list',
                               '--contains', tag,
                               '{}/*'.format(remote))).replace(' ', '').split(os.linesep)

    """ Show the most recent tag starting from a given commit"""
    def show_most_recent_tag(self, commit):
        return unicode(self.git.describe('--tags', '--abbrev=0', commit))

    """Returns a list of git commits between start (excluded) and end (included)"""
    def list_commits(self, start, end):
        return unicode(self.git.log('--pretty="%h"', start,
                       end)).replace('"', '').split(os.linesep)

    """Returns a list of tags names starting from a given commit.
       If the start_tag is given, it will filter out tags before start_tag"""
    def list_tags_names(self, commit, start_tag=None):
        if start_tag:
            return unicode(self.git.tag(
                '--contains', start_tag,
                '--sort=-committerdate',
                '--merged',
                commit)).split(os.linesep)
        else:
            return unicode(self.git.tag(
                '--sort=-committerdate',
                '--merged',
                commit)).split(os.linesep)

    """Returns a list of tags starting from a given commit.
       If the start_tag is given, it will filter out tags before start_tag"""
    def list_tags_commit_hashes(self, commit, start_tag=None):
        tags_commit_hashes = list()
        # TODO: Find a way to make git return the hash of the commit the tags
        # are pointing to (instead of the tag names or hash of the tags) in the
        # git commands used in list_tag_names
        for tag in self.list_tags_names(commit, start_tag):
            tags_commit_hashes.append(self.commit_hash(tag))
        return tags_commit_hashes

    """Prints the git commit in oneline format"""
    def show_oneline_commit(self, commit):
        return unicode(self.git.log('--oneline', '-1', commit))

    """Prints the git commit hash"""
    def commit_hash(self, commit):
        return unicode(self.git.log('--oneline', '-1', '--pretty="%h"', commit)).replace('"', '')

    """returns the hash of a file for a given commit"""
    def hash_file(self, commit, path):
        # with -l, according to man (git-ls-tree), the output format changes from:
        # <mode> SP <type> SP <object> TAB <file> to:
        # <mode> SP <type> SP <object> SP <object size> TAB <file>
        # This enables to more easily get the object has since it's surrounded by
        # spaces
        return unicode(self.git.ls_tree(commit, '-l', '--', path)).split(' ')[2]
