#!/usr/bin/env python2
# We use python2 because gitpython was only available in python2
# at the time of writing
#
# Copyright (C) 2018 Denis 'GNUtoo' Carikli <GNUtoo@makefreedom.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import os
import shlex
import sys
import copy
import time
from git import Repo
from git_commands import *
from git_history import *

git = git_commands()

######### Global configuration #########
verbose = False

######### Program logic #########
def main():
    parser = argparse.ArgumentParser(description='Reconstruct git history')
    parser.add_argument('-o', '--output_log')
    parser.add_argument('-t', '--start_tag', required=True)
    parser.add_argument('-v', '--verbose', action='store_true', default=False)

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-r', '--remote')
    group.add_argument('-b', '--branches', nargs='+')

    args = parser.parse_args()

    # Workaround the fact that shlex.quote() is not available
    # in python2. TODO: Fix that either by finding a way
    # to quote arguments in python2 or porting gitpython
    # to python3
    def shlex_quote(string):
        return string
    try:
        shlex.quote('')
    except:
        shlex.quote = shlex_quote

    if args.verbose:
        global verbose
        verbose = True
        sys.argv.pop(1)

    history = GitHistory(git, verbose,
                         shlex.quote(args.start_tag),
                         shlex.quote(args.remote),
                         shlex.quote(args.branches))

    history.build_branch_list()
    history.build_commit_list()
    history.build_unmodified_file_paths()
    history.check_commits()
    history.print_results()

    if args.output_log:
        history.write_results(shlex.quote(args.output_log))

if __name__ == '__main__':
    main()
