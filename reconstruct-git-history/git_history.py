#!/usr/bin/env python2
#
# Copyright (C) 2018 Denis 'GNUtoo' Carikli <GNUtoo@makefreedom.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
from git_commands import *

class GitHistory():
    def __init__(self, git, verbose, base_tag, remote=None, branches=None):
        self.git = git
        self.verbose = verbose

        self.base_tag_name = base_tag
        self.base_tag_commit_hash = self.git.commit_hash(base_tag)
        self.remote = remote
        self.branches = branches

        self.commits = set()
        self.paths = set()

    """ Returns True if it's a candidate branch, returns False otherwise"""
    def check_branch(self, branch):
        if self.verbose:
            print("{0}: checking branch {1}".format(time.ctime(), branch))

        # If the last branch tag is the base tag we don't need to do any more
        # research as this is a candidate branch
        if self.base_tag_commit_hash == self.git.commit_hash(
                self.git.show_most_recent_tag(branch)):
            if self.verbose:
                print("{0}: branch {1}: Last tag == base tag => Add".format(
                    time.ctime(), branch))
            return True

        # If we don't have the base tag we're looking for in the branch, we can
        # safely ditch the branch
        if self.base_tag_commit_hash not in self.git.list_tags_commit_hashes(
                branch, self.base_tag_commit_hash):
            if self.verbose:
                print("{0}: branch {1}: base tag not present in tag list => Remove".format(
                    time.ctime(), branch))
            return False

        # At the end we check if the last upstream tag in the branch is the base tag
        for tag_commit_hash in self.git.list_tags_commit_hashes(branch):
            if self.verbose:
                print("{0}: branch {1}: tag_commit_hash:{2} self.base_tag_commit_hash:{3}".format(
                    time.ctime(), branch, tag_commit_hash, self.base_tag_commit_hash))

            if tag_commit_hash not in self.base_tag_commit_hash:
                if self.verbose:
                    print("{0}: branch {1}: branch tag not upstream => continue".format(
                        time.ctime(), branch))
                continue

            elif tag_commit_hash != self.base_tag_commit_hash:
                if self.verbose:
                    print("{0}: branch {1}: Last upstream tag != base tag => Remove".format(
                        time.ctime(), branch))
                return False
            else:
                if self.verbose:
                    print("{0}: branch {1}: Last upstream tag == base tag => Add".format(
                        time.ctime(), branch))
                return True

        if self.verbose:
            print("{0}: branch {1}: No upstream tag found => Remove".format(
                time.ctime(), branch))

        return False

    """ Filter out the non-candidates branches"""
    def build_branch_list(self):
        print("{0}: building branch list".format(time.ctime()))
        # We assume that the users did their homework if they added branches manually
        if not self.remote:
            return

        self.branches = self.git.list_branches_with_tag(self.remote, self.base_tag_name)

        # The candidate branches are supposed to have the base tag as last upstream tag
        # so filter out the branches that don't
        all_branches = copy.copy(self.branches)
        branch_nr = 0
        total_branches = len(self.branches)
        print("{0}: {1} branche(s)".format(time.ctime(), total_branches))
        for branch in all_branches:
            branch_nr += 1
            print("{0}: checking branche(s): {1}/{2}".format(time.ctime(),
                                                              branch_nr,
                                                              total_branches))
            if not self.check_branch(branch):
                self.branches.remove(branch)


    """Build a list of all commits to check, returns a set of commit hashes"""
    def build_commit_list(self):
        print("{0}: building commit list".format(time.ctime()))

        # Add all commits since the tag
        branch_nr = 0
        total_branches = len(self.branches)
        for branch in self.branches:
            branch_nr += 1
            print("{0}: building commit list for branch: {1}/{2})".format(
                time.ctime(),
                branch_nr,
                total_branches))

            for commit in self.git.list_commits(self.base_tag_commit_hash, branch):
                self.commits.add(commit)

        if self.verbose:
            print("Commit list:")
            for commit in self.commits:
                print("  " + self.git.show_oneline_commit(commit))
        else:
            print("{0}: {1} commit(s)".format(time.ctime(), len(self.commits)))

    """build the list of file paths that are unmodified
    by the device vendor. Only the files that are identical
    between the device vendor and any of the SOC vendor commits
    are considered unmodified."""
    def build_unmodified_file_paths(self):
        final_paths = set()
        print("{0}: building path list".format(time.ctime()))

        # check if the paths are unmodified
        total_commits = len(self.commits)
        commit_nr = 0
        for commit in self.commits:
            commit_nr += 1
            print("{0}: building unmodified paths list for commit {1}/{2})".format(
                time.ctime(),
                commit_nr,
                total_commits))

            for path in self.git.list_commit_files(commit):
                try:
                    if self.git.hash_file('HEAD', path) == self.git.hash_file(commit, path):
                        self.paths.add(path)
                        if self.verbose:
                            print("{0}: {1}: HEAD == {2}".format(path,
                                                                 self.git.hash_file(commit, path),
                                                                 self.git.show_oneline_commit(commit)))
                except:
                    pass

        if self.verbose:
            print("{0}: paths: {1}".format(time.ctime(), self.paths))
        else:
            print("{0}: {1} paths to check".format(time.ctime(), len(self.paths)))

    """Rule out commits that have a file that differs with one
    that is unmodified by the device vendor"""
    def check_commits(self):

        final_commits = copy.copy(self.commits)

        total_commits = len(self.paths)
        commit_nr = 0
        for commit in self.commits:
            commit_nr += 1
            print("{0}: checking commit {1}/{2} for unmodified files".format(
                time.ctime(),
                commit_nr,
                total_commits))

            for path in self.paths:
                if self.verbose:
                    print("{0}: checking {1}".format(time.ctime(), path))
                try:
                    if self.git.hash_file('HEAD', path) != self.git.hash_file(commit, path):
                        if self.verbose:
                            print("{0}: {1} Removing {2}".format(time.ctime(), path,
                                                                 self.git.show_oneline_commit(commit)))
                            final_commits.remove(commit)
                    else:
                        if self.verbose:
                            print("{0}: {1}: Keeping {2}".format(time.ctime(), path,
                                                                 self.git.show_oneline_commit(commit)))
                        else:
                            pass
                except:
                    pass

        self.commits = final_commits

    def print_results(self):
        # Print the result
        print("Candidates:")
        for commit in self.commits:
            print(self.git.show_oneline_commit(commit))

    def write_results(self, path):
        outfile = open(path, 'w')
        for commit in self.commits:
            outfile.write(commit + os.linesep)
